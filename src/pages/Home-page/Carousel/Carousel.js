import React from 'react';
import { Navigation, Pagination, Scrollbar, A11y } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCardItem from '../swiper-card-item/swiper-card-item'
import 'swiper/css';
import 'swiper/css/navigation';



const Carousel = () => {

  return (
      <Swiper
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={50}
          slidesPerView={3}
          navigation
          pagination
          breakpoints={{
            420: {
              slidesPerView: 1,
            },
            750: {
              slidesPerView: 2,
            },
            1500: {
              slidesPerView: 3,
              spaceBetween: 50,
            }
          }}
         
          onSwiper={(swiper) => console.log(swiper)}
          onSlideChange={() => console.log('slide change')}

      
      >
        
        <SwiperSlide><SwiperCardItem /></SwiperSlide>
        <SwiperSlide><SwiperCardItem /></SwiperSlide>
        <SwiperSlide><SwiperCardItem /></SwiperSlide>
        <SwiperSlide><SwiperCardItem /></SwiperSlide>
        <SwiperSlide><SwiperCardItem /></SwiperSlide>
        <SwiperSlide><SwiperCardItem /></SwiperSlide>

      </Swiper>
  );
};

export default Carousel;