import React from 'react';
import { NavLink } from 'react-router-dom';
import './swiper-card-item.css'
import kepka from '../foto/mini_red.png'


const SwiperCardItem = () => {
   return (
    <NavLink className='sourceSwiper' to='/catalog/'>
        <div className='swiper-card'>
             <img className='card-img' src={kepka} alt="kepka"/>
             <div className="card-body">
                <div className='kepka-brand'>Adidas</div>
                <div className='kepka-model'>San Francisco Baseball</div>
                <div className="kepka-price">3800c</div>
             </div>
        </div>
    </NavLink>
   );
};

export default SwiperCardItem