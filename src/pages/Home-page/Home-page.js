import React from 'react'
import './Home-page.css'
import red from "./foto/red.svg";
import tree_caps from "./foto/tree_caps.png";
import { Link } from 'react-router-dom'
import Carousel from "./Carousel/Carousel";

const HomePage = () => {

    return (
        <div className='page'>
            <div className="firstSection">
                <div className="container">
                    <div className='hm_content'>
                        <h1>NEW ERA</h1>
                        <p>Новая коллекция 2021 уже доступны на заказ
                        яркие цвета, винтажный принт 70х, тематические
                        группы и отличное качество</p>
                            <Link to='/catalog' className='btn-hm'>Открыть каталог</Link>
                    </div>
                </div>
            </div>

            <div className='second_section'>
                <div className='container'>
                    <div className='wrapper_recomen'>
                        <Link to='/catalog' className='src_home'>
                        <div className='recommendations'>
                            <div className='photo_rec'>
                                <img src={red} alt='red'></img>
                            </div>
                            <div className='description_rec'>
                                <span>2021</span>
                                <h4 className='rec_name'>New Era</h4>
                                <span>Houston Rockets</span>
                                <div className='price'>
                                    <h5>2400c</h5>
                                </div>
                            </div>
                        </div>
                        </Link>
                        <Link to='/catalog' className='src_home'>
                        <div className='recommendations'>
                            <div className='photo_rec'>
                                <img src={red} alt='red'></img>
                            </div>
                            <div className='description_rec'>
                                <span>2021</span>
                                <h4 className='rec_name'>New Era</h4>
                                <span>Houston Rockets</span>
                                <div className='price'>
                                    <h5>2400c</h5>
                                </div>
                            </div>
                        </div>
                        </Link>
                        <Link to='/catalog' className='src_home'>
                        <div className='recommendations'>
                            <div className='photo_rec'>
                                <img src={red} alt='red'></img>
                            </div>
                            <div className='description_rec'>
                                <span>2021</span>
                                <h4 className='rec_name'>New Era</h4>
                                <span>Houston Rockets</span>
                                <div className='price'>
                                    <h5>2400c</h5>
                                </div>
                            </div>
                        </div>
                        </Link>
                    </div>
                    <div className='title'>
                        <h1>TOP SELLERS</h1>
                        <div className='mini-dot'></div>
                    </div>
                    <Carousel />
                </div>


            </div>

            <div className='third_section'>
                <div className='container'>
                    <div className='third'>
                        <div className='left_content_fourth'>
                            <Link to='/catalog' className='hm_src' >
                                <div className='recom'>
                                    <div className='description_rec'>
                                        <h4 className='rec_name'>New Era</h4>
                                        <span className='des'>Liberty</span>
                                        <div className='price_cap'>
                                            <h5>2400c</h5>
                                        </div>
                                    </div>
                                    <div className='photo_rec'>
                                    <img src={red} alt='red'></img>
                                    </div>
                                </div>
                            </Link>
                            <Link to='/catalog' className='hm_src'>
                                <div className='recom'>
                                    <div className='description_rec'>
                                        <h4 className='rec_name'>New Era</h4>
                                        <span className='des'>Liberty</span>
                                        <div className='price_cap'>
                                            <h5>2400c</h5>
                                        </div>
                                    </div>
                                    <div className='photo_rec'>
                                    <img src={red} alt='red'></img>
                                    </div>
                                </div>
                            </Link>
                            <Link to='/catalog' className='hm_src'>
                                <div className='recom'>
                                    <div className='description_rec'>
                                        <h4 className='rec_name'>New Era</h4>
                                        <span className='des'>Liberty</span>
                                        <div className='price_cap'>
                                            <h5>2400c</h5>
                                        </div>
                                    </div>
                                    <div className='photo_rec'>
                                    <img src={red} alt='red'></img>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div className='right_content_fourth'>
                            <div className='title_fourth'>
                                <h1>SUPREME & NEW ERA</h1>
                                <span>collaboration</span>
                            </div>
                            <Link to='/catalog' className='btn_home_third'>Открыть каталог</Link>
                            <img src={tree_caps} alt='' className='tree'></img>
                        </div>
                    </div>


                </div>
            </div>

            <div className='fourth_section'>
                <div className='container'>
                    <h1>CUSTOM CUPS в цифрах</h1>
                    <div className='result'>
                        <div className='sales'>
                            <div>12000</div>
                            <p>проданных кепок</p>
                        </div>
                        <div className='sales'>
                            <div>9</div>
                            <p>проданных кепок</p>
                        </div>
                        <div className='sales'>
                            <div>8500</div>
                            <p>проданных кепок</p>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        
    )
}

export default HomePage