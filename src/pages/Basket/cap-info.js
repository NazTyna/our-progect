import React,{useState, useEffect} from 'react'
import './cap-info.css'
import cap_img1 from './photo/cap.png'
import Modal  from "./modal/modal"
import modal_img from './photo/checkpoint.svg'
import { Link } from 'react-router-dom'
import { IoIosArrowForward } from "react-icons/io";

const CapInfo = () => {
    const [modalActive, setModalActive] = useState(false)
    const [toggleState, setToggleState] = useState(1);
    const [quantity, setCount] = useState(0);

    useEffect(() => {
        console.log(toggleState)
    }, [toggleState])

    const toggleAdapter = (size) => {
        setToggleState(1)
    }

    const decrement = () => {
        if(quantity > 1){
            setCount(prevCount => prevCount - 1);
        }
    }
    const increment = () => {
        if(quantity < 100){
            setCount(prevCount => prevCount + 1);
        }
    }

    return (
        <div className='wrapper_basket'>
            <ul>
                    <li>
                        <Link to={"/"} activeStyle={{color:"yellow"}} >Home<IoIosArrowForward/></Link>
                    </li>
                    <li>
                        <Link to="/basket" activeClassName={"active"} >Карзина<IoIosArrowForward/></Link>
                        </li>
                </ul>
            <div className='basket'>
                <div className='container'>
                <div className='wrapper__cap'>
                    <div className='trait_black'></div>
                    <div className='cap-info'>
                        <div className='cap_img'>
                            <img src={cap_img1} alt='cap' />
                        </div>
                        <div className='counter_cap'>
                            <div className='minus black' onClick={decrement}>
                                <button className='btn_none'>-</button>
                            </div>
                            <div className='count' >{quantity}</div>
                            <div className='plus black' onClick={increment}>
                                <button className='btn_none'>+</button>
                            </div>
                        </div>
                        <div className='changed_size'>
                            <input className={toggleState === 1 ? "adapter adapter-active" : "adapter"}
                            type="button" value="L" onClick={(size) => toggleAdapter(4)} onChange="size"/>
                        </div>
                        <div className='name_cap'>
                            <h3>NEW ERA</h3>
                            <p>BLACK SNAPBACK 59 FIFTY</p>
                        </div>
                        <div className='price_cap'>
                            <h4>3200сом</h4>
                        </div>
                    </div>
                    <div className='trait_black'></div>
                </div>
                <div className='wrapper__info'>
                    <div className='input_info'>
                        <h2>Ваша информация</h2>
                        <div className='inputs'>
                            <input type='text' placeholder='Имя' className='input name'/>
                            <input type='text' placeholder='Фамилия' className='input surname'/>
                            <input type='number' placeholder='+996 777 888 999' className='input number'/>
                            <input type='text' placeholder='Email' className='input email'/>
                            <input type='text' placeholder='Адрес' className='input adress'/>
                        </div>
                    </div>
                    <div className='buy'>
                        <button className='btn btn-buy' onClick={() => {
                            setModalActive(true)
                            console.log('hello')
                            }}>Купить</button>
                        
                    </div>
                    
                    <Modal active={modalActive} setActive={setModalActive} >
                        <div className='modal_text'>
                            <img src={modal_img} alt='' className='modal_img'></img>
                            <p>Спасибо<br></br> Ваш заказ принят<br></br> Ожидайте с Вами скоро свяжутся</p>
                            <div className='modal_btn'>
                                <Link to='/' className='btn_modal'>На главную</Link>
                            </div>
       
                        </div>
                    </Modal>
                </div>
                </div>
            </div>
        </div>
    )
}

export default CapInfo