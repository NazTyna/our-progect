import cap from './cap.svg';
import red_catalog from './red_catalog.png';
import supreme_mbl from './supreme_mbl.png';
import nba from './nba.png';
import chicago6 from './chicago6.png';
import maroon7 from './maroon7.png';
import supreme8 from './supreme8.png';


// eslint-disable-next-line import/no-anonymous-default-export
export default { cap, red_catalog, supreme_mbl, nba, chicago6, maroon7, supreme8 };