import React from "react";
import  './Catalog.css';
import cursor_right from './photo/cursor_right.svg';
import vector from './photo/vector.svg';
import { Link } from 'react-router-dom';
import { IoIosArrowForward } from "react-icons/io";


function Card(props) {

   const onClickButton = () => {
     alert(props.title);
   };

    return (
            <div className="Card-Items">
                <ul>
                    <li>
                        <Link to={"/"} activeClassName={"active"} >Home<IoIosArrowForward/></Link>
                    </li>
                    <li>
                        <Link to="/details" className={"active"} >Catalog</Link>
                        </li>
                </ul>

            <div className="Baner">
                <h1 className="text_laren">НОВАЯ СЕРИЯ<br></br>
                                           McLAREN
                                       </h1>
            </div>
            <div className="second-block">

            <div className="container">
            <div className='drawer'>

            <div className='cartItem'>

            <div className='text'>
                <ul>
                <li>Популярные</li>
                <li>Сначалa дешевые</li>
                <li>Сначалa дорогие</li>
                <li>Новинки</li>
                </ul>
            </div>
            <div className='vector_catalog'>
            <img src={vector} alt='vector.svg'/>
                        </div>

            </div>
            </div>

                <div className="Filter-card" >
                        <div className='Compact-down' />
                         </div>
                    <div className="Card-list">
                        { props.arr.map(({id, imgUrl,name,price,title }) => (
                            <div key={id} className='Card' onClick={onClickButton}>
                                <div className='Card-img'>
                                    <img src={imgUrl} alt={name}/>
                                </div>
                                <div className="Info_Card">
                                    <div className="Card-description">
                                        <h4>{name}</h4>
                                        <p>{title}</p>
                                    </div>
                                    <div className="Cap_price">
                                        <h4>{price}</h4>
                                    </div>
                                </div>
                            </div>

                        ))}
                    </div>
                    <div className='switches'>
                       <a href='/##'> 1</a>
                        <a href='##'>2</a>
                        <a href='##'>3</a>
                        <a href='##'>4</a>
                        <a href='##'>5</a>
                        <div className="cursor_right">
                        <img src={cursor_right} alt='cursor_right'/>
                        </div>
                        <div className="switches-logo"/>
                    </div>
            </div>
                        </div>

        </div>

    )
}
export default Card;