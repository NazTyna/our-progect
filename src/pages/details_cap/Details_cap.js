import React, {useState, useEffect} from 'react'
import './Details_cap.css'
import back_cap from '../../photo/back_cap.png'
import side_cap from '../../photo/side_cap.png'
import inside_cap from '../../photo/inside_cap.png'
import Carousel from '../Home-page/Carousel/Carousel'
import big_cap from '../../photo/ganerely_cap.png'
import { Link } from 'react-router-dom'
import { IoIosArrowForward } from "react-icons/io";


const DetailsCap = () => {
    const [toggleState, setToggleState] = useState(1);
    const [quantity, setCount] = useState(0);

    useEffect(() => {
        console.log(toggleState)
    }, [toggleState])

    const toggleAdapter = (index) => {
        setToggleState(index)
    }
    const decrement = () => {
        if(quantity > 1){
            setCount(prevCount => prevCount - 1);
        }
    }
    const increment = () => {
        if(quantity < 100){
            setCount(prevCount => prevCount + 1);
        }
    }

    return (

    <div className="container">
        <ul>
            <li>
                <Link to={"/"} activeClassName={"active"} >Home<IoIosArrowForward/></Link>
            </li>
            <li>
                <Link to="/catalog" activeClassName={"active"} >Catalog<IoIosArrowForward/></Link>
            </li>
            <li>
                <Link to="/details" activeClassName={"active"}>Details cap</Link>
            </li>
        </ul>
        <div className='wrapper_details'>
            <div className='details_cap'>
                <div className='left_content'>
                    <div className='big_img'>
                        <img className='big_capp' src={big_cap} alt='' />
                    </div>
                    <div className='down_img'>
                        <div className='small_img'>
                            <img className='small_img_basket' src={back_cap} alt='' />
                        </div>
                        <div className='small_img'>
                            <img className='small_img_basket' src={side_cap} alt='' />
                        </div>
                        <div className='small_img'>
                            <img className='small_img_basket' src={inside_cap} alt='' />
                        </div>
                    </div>
                </div>
                <div className='right_content'>
                    <div className='name_cap'>
                        <h3>New ERA</h3>
                        <p>BLACK SNAPBACK 59 FIFTY</p>
                    </div>
                    <div className='size'>
                        <input className={toggleState === 1 ? "adapter adapter-active" : "adapter"}
                               type="button" value='S' onClick={() => toggleAdapter(1)}/>
                        <input className={toggleState === 2 ? "adapter adapter-active" : "adapter"}
                               type="button" value='M' onClick={() => toggleAdapter(2)}/>
                        <input className={toggleState === 3 ? "adapter adapter-active" : "adapter"}
                               type="button" value='L' onClick={() => toggleAdapter(3)}/>
                        <input className={toggleState === 4 ? "adapter adapter-active" : "adapter"}
                               type="button" value='XL' onClick={() => toggleAdapter(4)}/>
                    </div>
                    <div className='counter'>
                        <div className='minus black' onClick={decrement}>
                            <button className='btn_none'>-</button></div>
                        <div className='count' >{quantity}</div>
                        <div className='plus black' onClick={increment}>
                            <button className='btn_none' >+</button></div>
                    </div>
                    <div className='sale'>
                        <div className='price_right_sale'>3200сом</div>
                        <div className='btn_test'>
                            <button className='btn'>
                                Добавить в корзину</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className='similar-items'>
                <h1>Похожие товары</h1>
            </div>
            <Carousel />
            <div className='btn_btn'>
                <p>Товар добавлен в корзину</p>
            </div>
        </div>
    </div>

    )
}

export default DetailsCap