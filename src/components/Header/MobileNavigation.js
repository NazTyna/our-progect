import React from 'react';
import HeaderLinks from "./HeaderLinks";
import "./MobileNavigation.css"
import { MdMenu } from 'react-icons/md';
import {useState} from "react";

const MobileNavigation = () => {

    const [open, setOpen] = useState(false);

    return (
        <nav className='MobileNavigation'>
            <MdMenu className='Hamburger' size='40px'
                    onClick={() => setOpen(!open)}
            />
            {open && <HeaderLinks/>}
        </nav>
    );
};

export default MobileNavigation;