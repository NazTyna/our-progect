import React from 'react';
import HeaderLinks from "./HeaderLinks";
import './Navigation.css'

const Navigation = () => {
    return (
        <nav className='Navigation'>
            <HeaderLinks/>
        </nav>
    );
};

export default Navigation;