import React from 'react';
import './HeaderLinks.css';

const HeaderLinks = () => {
    return (
        <ul>
            <li className='navigat'>
                <a href="/catalog" className='header_src'>Каталог</a>
            </li>
            <li>
                <a href="/catalog" className='header_src' >Бренды</a>
            </li>
            <li>
                <a href="/" className='header_src' >О нас</a>
            </li>
        </ul>
    );
};

export default HeaderLinks;