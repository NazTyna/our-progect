import React from 'react'
import './Header.css'
import logo from '../../photo/logo.svg'
import logo__basket from '../../photo/logo__basket.svg'
import { Link } from 'react-router-dom'
import Navigation from "./Navigation";
import MobileNavigation from "./MobileNavigation";

const AppHeader = (props) => {

//    const[value, setValue]=useState('')
//    const onChangeHandler = (inputText) =>{
//    setValue(inputText)
//    props.onChange(value)
//    }

    return (
        <div>
            <div className='App-header'>
                <Link to='/'><img src={logo} alt='' /></Link>
                    <Navigation/>
                    <MobileNavigation/>
                <div className='header__section'>
                    <div className='header__item'>
                        <form className='header__item input__search'>
                            <input type='text' className=' search logo__loupe'/>
                        </form>
                    </div>
                    <div className='header__item'>
                        <Link to='/basket' className="down_header_src logo__basket">
                            <img src={logo__basket} alt=''  /></Link>
                    </div>
                </div>
            </div>
        </div>



    )
}
export default AppHeader