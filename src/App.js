import React, {useState}  from "react";
import './App.css';
import DetailsCap from "./pages/details_cap/Details_cap";
import Header from "./components/Header/Header";
import Catalog from "./pages/Catalog/Catalog";
import CapInfo from "./pages/Basket/cap-info";
import HomePage from "./pages/Home-page/Home-page";
import Footer from "./components/Footer/components";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import img from "./pages/Catalog/photo/index";

function App() {


const arr = [
    {
        id: 1,
        name: 'Nike',
        price: 4500,
        imgUrl: img.cap,
        title: 'French Fries Series'
    },
    {
        id: 2,
        name: 'New Era',
        price: "3800c",
        imgUrl: img.red_catalog,
        title: 'New York Yankies'
    },
    {
        id: 3,
        name: 'New Era',
        price: 3990,
        imgUrl: img.nba,
        title: 'Alpha Male Team'
    },
    {
        id: 4,
        name: 'New Era',
        price: 6000,
        imgUrl: img.supreme_mbl,
        title: 'Supreme Collab'
    },


     {
         id: 6,
          name: 'Reebok',
          price: 6000,
          imgUrl: img.chicago6,
          title: 'Chicago Bulls'
     },
     {
          id: 7,
          name: 'Flexfit',
          price: 6000,
          imgUrl: img.maroon7,
          title: 'One Maroon'
     },

     {
          id: 8,
          name: 'New Era',
          price: 6000,
          imgUrl: img.supreme8,
          title: 'Supreme Collab Red'
     }
]
    const [caps, setCaps] = useState(arr)
    const filteredCard = (text) =>{
    setCaps((oldState)=>{
    const filteredArr = oldState.filter(item => item.title.toLowerCase().includes(text.toLowerCase()))
    return filteredArr
})

}
  return (
    <BrowserRouter>
        <Header OnChange={filteredCard} />
        <Routes>
            <Route path="/" element={<HomePage/>}/>
            <Route path="/catalog" element={<Catalog arr={caps} />}/>
            <Route path="/details" element={<DetailsCap />}/>
            <Route path="/basket" element={<CapInfo />}/>
        </Routes>
        <Footer/>
    </BrowserRouter>
  );
}

export default App;


